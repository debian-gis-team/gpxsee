<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>CadenceGraph</name>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="11"/>
        <source>rpm</source>
        <translation>rpm</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="12"/>
        <location filename="../src/GUI/cadencegraph.h" line="13"/>
        <source>Cadence</source>
        <translation>Kadans</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
</context>
<context>
    <name>CadenceGraphItem</name>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="29"/>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="31"/>
        <source>rpm</source>
        <translation>rpm</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
</context>
<context>
    <name>Data</name>
    <message>
        <location filename="../src/data/data.cpp" line="116"/>
        <source>Supported files</source>
        <translation>Desteklenen dosyalar</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>CSV files</source>
        <translation>CSV dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>FIT files</source>
        <translation>FIT dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>GPX files</source>
        <translation>GPX dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>IGC files</source>
        <translation>IGC dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>KML files</source>
        <translation>KML dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>LOC files</source>
        <translation>LOC dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="121"/>
        <source>NMEA files</source>
        <translation>NMEA dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="122"/>
        <source>OziExplorer files</source>
        <translation>OziExplorer dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>SLF files</source>
        <translation>SLF dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>TCX files</source>
        <translation>TCX dosyaları</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="124"/>
        <source>All files</source>
        <translation>Tüm dosyalar</translation>
    </message>
</context>
<context>
    <name>ElevationGraph</name>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="48"/>
        <location filename="../src/GUI/elevationgraph.h" line="13"/>
        <source>Elevation</source>
        <translation>Rakım</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="59"/>
        <source>Ascent</source>
        <translation>Tırmanış</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="61"/>
        <source>Descent</source>
        <translation>İniş</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="63"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="65"/>
        <source>Minimum</source>
        <translation>Asgari</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="127"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="130"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
</context>
<context>
    <name>ElevationGraphItem</name>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="37"/>
        <source>Ascent</source>
        <translation>Tırmanış</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="39"/>
        <source>Descent</source>
        <translation>İniş</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="41"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="43"/>
        <source>Minimum</source>
        <translation>Asgari</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>PDF files</source>
        <translation>PDF dosyaları</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>All files</source>
        <translation>Tüm dosyalar</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="52"/>
        <source>Portrait</source>
        <translation>Dikey</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="53"/>
        <source>Landscape</source>
        <translation>Yatay</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>in</source>
        <translation>inç</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="94"/>
        <source>Page Setup</source>
        <translation>Sayfa Düzeni</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="97"/>
        <source>Page size:</source>
        <translation>Sayfa boyutu:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="98"/>
        <source>Resolution:</source>
        <translation>Çözünürlük:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="99"/>
        <source>Orientation:</source>
        <translation>Yönlendirme:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="100"/>
        <source>Margins:</source>
        <translation>Kenar boşlukları:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="106"/>
        <source>File:</source>
        <translation>Dosya:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="113"/>
        <source>Output file</source>
        <translation>Çıktı dosyası</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="120"/>
        <source>Export</source>
        <translation>Dışa aktar</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="135"/>
        <source>Export to PDF</source>
        <translation>PDF olarak dışa aktar</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <source>No output file selected.</source>
        <translation>Çıkış dosyası seçilmedi.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <source>%1 is a directory.</source>
        <translation>%1 bir klasör.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>%1 is not writable.</source>
        <translation>%1 yazılabilir değil.</translation>
    </message>
</context>
<context>
    <name>FileSelectWidget</name>
    <message>
        <location filename="../src/GUI/fileselectwidget.cpp" line="38"/>
        <source>Select file</source>
        <translation>Dosya seç</translation>
    </message>
</context>
<context>
    <name>Format</name>
    <message>
        <location filename="../src/GUI/format.cpp" line="54"/>
        <location filename="../src/GUI/format.cpp" line="61"/>
        <location filename="../src/GUI/format.cpp" line="84"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="57"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="64"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="68"/>
        <location filename="../src/GUI/format.cpp" line="81"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="71"/>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>GUI</name>
    <message>
        <location filename="../src/GUI/gui.cpp" line="199"/>
        <source>Quit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="206"/>
        <location filename="../src/GUI/gui.cpp" line="693"/>
        <location filename="../src/GUI/gui.cpp" line="694"/>
        <source>Paths</source>
        <translation>Yollar</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="209"/>
        <location filename="../src/GUI/gui.cpp" line="659"/>
        <location filename="../src/GUI/gui.cpp" line="660"/>
        <source>Keyboard controls</source>
        <translation>Klavye kontrolleri</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="212"/>
        <location filename="../src/GUI/gui.cpp" line="637"/>
        <source>About GPXSee</source>
        <translation>Hakkında GPXSee</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="217"/>
        <source>Open...</source>
        <translation>Aç...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="222"/>
        <source>Print...</source>
        <translation>Yazdır...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="229"/>
        <source>Export to PDF...</source>
        <translation>PDF olarak dışa aktar...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="235"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="241"/>
        <source>Reload</source>
        <translation>Yeniden yükle</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="248"/>
        <source>Statistics...</source>
        <translation>İstatistikler...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="256"/>
        <source>Load POI file...</source>
        <translation>POI dosyası yükle...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="260"/>
        <source>Close POI files</source>
        <translation>POI dosyaları kapat</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="264"/>
        <source>Overlap POIs</source>
        <translation>POI&apos;leri üst üste getir</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="269"/>
        <source>Show POI labels</source>
        <translation>POI etiketlerini göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="274"/>
        <source>Show POIs</source>
        <translation>POI&apos;leri göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="284"/>
        <source>Show map</source>
        <translation>Haritayı göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="292"/>
        <source>Load map...</source>
        <translation>Harita yükle...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="296"/>
        <source>Clear tile cache</source>
        <translation>Döşeme önbelleğini temizle</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="301"/>
        <location filename="../src/GUI/gui.cpp" line="306"/>
        <location filename="../src/GUI/gui.cpp" line="676"/>
        <source>Next map</source>
        <translation>Sonraki harita</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="317"/>
        <source>Show tracks</source>
        <translation>İzleri göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="322"/>
        <source>Show routes</source>
        <translation>Rotaları göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="327"/>
        <source>Show waypoints</source>
        <translation>Yer işaretlerini göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="332"/>
        <source>Waypoint labels</source>
        <translation>Yer işareti etiketleri</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="337"/>
        <source>Route waypoints</source>
        <translation>Rota yer işaretleri</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="344"/>
        <source>Show graphs</source>
        <translation>Grafikleri göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="354"/>
        <location filename="../src/GUI/gui.cpp" line="1004"/>
        <location filename="../src/GUI/gui.cpp" line="1069"/>
        <source>Distance</source>
        <translation>Mesafe</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="361"/>
        <location filename="../src/GUI/gui.cpp" line="527"/>
        <location filename="../src/GUI/gui.cpp" line="1007"/>
        <location filename="../src/GUI/gui.cpp" line="1071"/>
        <source>Time</source>
        <translation>Zaman</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="368"/>
        <source>Show grid</source>
        <translation>Izgarayı göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="373"/>
        <source>Show slider info</source>
        <translation>Kaydırıcı bilgisi göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="380"/>
        <source>Show toolbars</source>
        <translation>Araç çubuklarını göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="387"/>
        <source>Total time</source>
        <translation>Toplam süre</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="393"/>
        <location filename="../src/GUI/gui.cpp" line="1009"/>
        <location filename="../src/GUI/gui.cpp" line="1073"/>
        <source>Moving time</source>
        <translation>Hareket zamanı</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="401"/>
        <source>Metric</source>
        <translation>Metrik</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="407"/>
        <source>Imperial</source>
        <translation>Emperyal</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="413"/>
        <source>Nautical</source>
        <translation>Denizcilik</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="421"/>
        <source>Decimal degrees (DD)</source>
        <translation>Desimal derece (DD)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="427"/>
        <source>Degrees and decimal minutes (DMM)</source>
        <translation>Derece ve desimal dakika (DDD)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="434"/>
        <source>Degrees, minutes, seconds (DMS)</source>
        <translation>Derece, dakika, saniye (DDS)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="440"/>
        <source>Fullscreen mode</source>
        <translation>Tam ekran modu</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="447"/>
        <source>Options...</source>
        <translation>Seçenekler...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="453"/>
        <source>Next</source>
        <translation>Sonraki</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="457"/>
        <source>Previous</source>
        <translation>Önceki</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="461"/>
        <source>Last</source>
        <translation>Son</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="465"/>
        <source>First</source>
        <translation>İlk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="473"/>
        <source>&amp;File</source>
        <translation>&amp;Dosya</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="488"/>
        <source>&amp;Map</source>
        <translation>&amp;Harita</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="496"/>
        <source>&amp;Graph</source>
        <translation>&amp;Grafik</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="505"/>
        <source>&amp;POI</source>
        <translation>&amp;POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="506"/>
        <source>POI files</source>
        <translation>POI dosyalar</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="517"/>
        <source>&amp;Data</source>
        <translation>&amp;Veri</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="518"/>
        <source>Display</source>
        <translation>Görüntüleme</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="526"/>
        <source>&amp;Settings</source>
        <translation>&amp;Ayarlar</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="530"/>
        <source>Units</source>
        <translation>Birimler</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="534"/>
        <source>Coordinates format</source>
        <translation>Koordinat biçimi</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="544"/>
        <source>&amp;Help</source>
        <translation>&amp;Yardım</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="560"/>
        <source>File</source>
        <translation>Dosya</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="567"/>
        <source>Show</source>
        <translation>Göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="573"/>
        <source>Navigation</source>
        <translation>Navigasyon</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="638"/>
        <source>Version %1</source>
        <translation>Sürüm %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="642"/>
        <source>GPXSee is distributed under the terms of the GNU General Public License version 3. For more info about GPXSee visit the project homepage at %1.</source>
        <translation>GPXSee, GNU Genel Kamu Lisansı sürüm 3 şartlarına göre dağıtılır. GPXSee hakkında daha fazla bilgi için %1 proje ana sayfasını ziyaret edin.</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="663"/>
        <source>Next file</source>
        <translation>Sonraki dosya</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="664"/>
        <source>Previous file</source>
        <translation>Önceki dosya</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="666"/>
        <source>First file</source>
        <translation>İlk dosya</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="668"/>
        <source>Last file</source>
        <translation>Son dosya</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="669"/>
        <source>Append file</source>
        <translation>Dosya ekle</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="670"/>
        <source>Next/Previous</source>
        <translation>Sonraki/Önceki</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="672"/>
        <source>Toggle graph type</source>
        <translation>Geçiş grafik türü</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="674"/>
        <source>Toggle time type</source>
        <translation>Geçiş zaman türü</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="678"/>
        <source>Previous map</source>
        <translation>Önceki harita</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="679"/>
        <source>Zoom in</source>
        <translation>Yaklaş</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="681"/>
        <source>Zoom out</source>
        <translation>Uzaklaş</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="683"/>
        <source>Digital zoom</source>
        <translation>Sayısal zum</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="684"/>
        <source>Zoom</source>
        <translation>Zum</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="697"/>
        <source>Map directory:</source>
        <translation>Harita klasörü:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="699"/>
        <source>POI directory:</source>
        <translation>POI klasörü:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="701"/>
        <source>GCS/PCS directory:</source>
        <translation>GCS/PCS klasörü:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="703"/>
        <source>Tile cache directory:</source>
        <translation>Döşeme önbellek klaörü:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="712"/>
        <source>Open file</source>
        <translation>Dosya aç</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="806"/>
        <source>Error loading data file:</source>
        <translation>Veri dosyası yüklenirken hata oluştu:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="809"/>
        <location filename="../src/GUI/gui.cpp" line="844"/>
        <source>Line: %1</source>
        <translation>Satır: %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="817"/>
        <source>Open POI file</source>
        <translation>POI dosyası aç</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="841"/>
        <source>Error loading POI file:</source>
        <translation>POI dosyası yüklenirken hata oluştu:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="981"/>
        <location filename="../src/GUI/gui.cpp" line="1049"/>
        <source>Tracks</source>
        <translation>İzler</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="984"/>
        <location filename="../src/GUI/gui.cpp" line="1051"/>
        <source>Routes</source>
        <translation>Rotalar</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="987"/>
        <location filename="../src/GUI/gui.cpp" line="1053"/>
        <source>Waypoints</source>
        <translation>Yer işaretleri</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="993"/>
        <location filename="../src/GUI/gui.cpp" line="997"/>
        <location filename="../src/GUI/gui.cpp" line="1059"/>
        <location filename="../src/GUI/gui.cpp" line="1062"/>
        <source>Date</source>
        <translation>Tarih</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1030"/>
        <location filename="../src/GUI/gui.cpp" line="1031"/>
        <source>Statistics</source>
        <translation>İstatistikler</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1045"/>
        <source>Name</source>
        <translation>Adı</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1272"/>
        <source>Open map file</source>
        <translation>Harita dosyası aç</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1295"/>
        <source>Error loading map:</source>
        <translation>Harita yüklenirken hata oluştu:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1306"/>
        <source>No files loaded</source>
        <translation>Hiç dosya yüklenmedi</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/GUI/gui.cpp" line="1310"/>
        <source>%n files</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>GearRatioGraph</name>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="12"/>
        <location filename="../src/GUI/gearratiograph.h" line="14"/>
        <source>Gear ratio</source>
        <translation>Dişli oranı</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="22"/>
        <source>Most used</source>
        <translation>Sık kullanılan</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="24"/>
        <source>Minimum</source>
        <translation>Asgari</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="26"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
</context>
<context>
    <name>GearRatioGraphItem</name>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="44"/>
        <source>Minimum</source>
        <translation>Asgari</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="45"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="46"/>
        <source>Most used</source>
        <translation>Sık kullanılan</translation>
    </message>
</context>
<context>
    <name>GraphView</name>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="46"/>
        <source>Data not available</source>
        <translation>Data erişilemez</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="66"/>
        <location filename="../src/GUI/graphview.cpp" line="181"/>
        <source>Distance</source>
        <translation>Mesafe</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="111"/>
        <location filename="../src/GUI/graphview.cpp" line="119"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="114"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="122"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="127"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="130"/>
        <source>km</source>
        <translation>Km</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="136"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="139"/>
        <source>min</source>
        <translation>dk</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="142"/>
        <source>h</source>
        <translation>sa</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="183"/>
        <source>Time</source>
        <translation>Zaman</translation>
    </message>
</context>
<context>
    <name>HeartRateGraph</name>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="11"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="12"/>
        <location filename="../src/GUI/heartrategraph.h" line="13"/>
        <source>Heart rate</source>
        <translation>Nabız</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
</context>
<context>
    <name>HeartRateGraphItem</name>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="29"/>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="31"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
</context>
<context>
    <name>MapList</name>
    <message>
        <location filename="../src/map/maplist.cpp" line="117"/>
        <source>Supported files</source>
        <translation>Desteklenen dosyalar</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="119"/>
        <source>MBTiles maps</source>
        <translation>MBTiles haritalar</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="120"/>
        <source>Garmin JNX maps</source>
        <translation>Garmin JNX haritalar</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="121"/>
        <source>OziExplorer maps</source>
        <translation>OziExplorer haritalar</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="122"/>
        <source>TrekBuddy maps/atlases</source>
        <translation>TrekBuddy haritalar/atlaslar</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="123"/>
        <source>GeoTIFF images</source>
        <translation>GeoTIFF görüntü</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="124"/>
        <source>Online map sources</source>
        <translation>Online harita kaynakları</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="39"/>
        <source>Always show the map</source>
        <translation>Her zaman haritayı göster</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="42"/>
        <source>Show the map even when no files are loaded.</source>
        <translation>Hiç dosya yüklenmese dahi haritayı göster.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="45"/>
        <source>High-resolution</source>
        <translation>Yüksek çözünürlük</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="46"/>
        <source>Standard</source>
        <translation>Standart</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="51"/>
        <source>Non-HiDPI maps are loaded as HiDPI maps. The map is sharp but map objects are small/hard to read.</source>
        <translation>HiDPI olmayan haritalar, HiDPI haritaları olarak yüklenir. Harita keskin ama harita nesneleri küçük/okunması zor.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="53"/>
        <source>Non-HiDPI maps are loaded such as they are. Map objects have the expected size but the map is blurry.</source>
        <translation>HiDPI olmayan haritalar, oldukları gibi yüklenir. Harita nesneleri beklenen boyuta sahip ancak harita bulanıktır.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="86"/>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="88"/>
        <source>HiDPI display mode</source>
        <translation>HiDPI gösterim modu</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="105"/>
        <source>Base color:</source>
        <translation>Temel renk:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="106"/>
        <source>Palette shift:</source>
        <translation>Palet değişimi:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="108"/>
        <source>Colors</source>
        <translation>Renkler</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="119"/>
        <source>Track width:</source>
        <translation>İz genişliği:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="120"/>
        <source>Track style:</source>
        <translation>İz tarzı:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="122"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="138"/>
        <source>Width:</source>
        <translation>Genişlik:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="123"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="139"/>
        <source>Style:</source>
        <translation>Tarz:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="124"/>
        <source>Tracks</source>
        <translation>İzler</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="135"/>
        <source>Route width:</source>
        <translation>Rota genişliği:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="136"/>
        <source>Route style:</source>
        <translation>Rota tarzı:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="140"/>
        <source>Routes</source>
        <translation>Rotalar</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="144"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="226"/>
        <source>Use anti-aliasing</source>
        <translation>Kenar yumuşatma kullan</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="176"/>
        <source>Waypoint color:</source>
        <translation>Yer işareti rengi:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="177"/>
        <source>Waypoint size:</source>
        <translation>Yer işareti boyutu:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="179"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="195"/>
        <source>Color:</source>
        <translation>Renk:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="180"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="196"/>
        <source>Size:</source>
        <translation>Boyut:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="181"/>
        <source>Waypoints</source>
        <translation>Yer işaretleri</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="192"/>
        <source>POI color:</source>
        <translation>POI renk:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="193"/>
        <source>POI size:</source>
        <translation>POI boyutu:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="197"/>
        <source>POIs</source>
        <translation>POI&apos;ler</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="223"/>
        <source>Line width:</source>
        <translation>Hat genişliği:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="224"/>
        <source>Slider color:</source>
        <translation>Kaydırıcı rengi:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="246"/>
        <source>Background color:</source>
        <translation>Arka plan rengi:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="247"/>
        <source>Map opacity:</source>
        <translation>Harita opaklığı:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="257"/>
        <source>Paths</source>
        <translation>Yollar</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="258"/>
        <source>Points</source>
        <translation>Noktalar</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="259"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="468"/>
        <source>Graphs</source>
        <translation>Grafikler</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="260"/>
        <source>Map</source>
        <translation>Harita</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="267"/>
        <source>Moving average window size</source>
        <translation>Ortalama pencere boyutuna taşıma</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="286"/>
        <source>Elevation:</source>
        <translation>Rakım:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="287"/>
        <source>Speed:</source>
        <translation>Hız:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="288"/>
        <source>Heart rate:</source>
        <translation>Nabız:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="289"/>
        <source>Cadence:</source>
        <translation>Kadans:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="290"/>
        <source>Power:</source>
        <translation>Güç:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="292"/>
        <source>Smoothing</source>
        <translation>Yumuşatma</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="296"/>
        <source>Eliminate GPS outliers</source>
        <translation>Aykırı GPS ele</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="302"/>
        <source>Outlier elimination</source>
        <translation>Aykırıyı eleme</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="309"/>
        <source>Smoothing:</source>
        <translation>Yumuşatma:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="327"/>
        <source>mi/h</source>
        <translation>mi/h</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="330"/>
        <source>kn</source>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="333"/>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="337"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="491"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="341"/>
        <source>Minimal speed:</source>
        <translation>Asgari hız:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="342"/>
        <source>Minimal duration:</source>
        <translation>Minimum süre:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="348"/>
        <source>Computed from distance/time</source>
        <translation>Mesafe/zamandan hesaplandı</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="349"/>
        <source>Recorded by device</source>
        <translation>Cihazdan kaydedilen</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="364"/>
        <source>Filtering</source>
        <translation>Filtreleme</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="365"/>
        <source>Pause detection</source>
        <translation>Duraklama algılama</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="366"/>
        <source>Speed</source>
        <translation>Hız</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="378"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="381"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="384"/>
        <source>km</source>
        <translation>Km</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="388"/>
        <source>POI radius:</source>
        <translation>POI yarıçapı:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="394"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="534"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="401"/>
        <source>WYSIWYG</source>
        <translation>WYSIWYG</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="402"/>
        <source>High-Resolution</source>
        <translation>Yüksek çözünürlük</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="407"/>
        <source>The printed area is approximately the display area. The map zoom level does not change.</source>
        <translation>Yazdırılan alan yaklaşık olarak görüntü alanıdır. Harita zum seviyesi değişmez.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="409"/>
        <source>The zoom level will be changed so that the whole content (tracks/waypoints) fits to the printed area and the map resolution is as close as possible to the print resolution.</source>
        <translation>Yakınlaştırma seviyesi, tüm içeriğin (izler/yer işaretleri) yazdırılan alana sığması ve harita çözünürlüğünün baskı çözünürlüğüne olabildiğince yakın olacak şekilde değiştirilecektir.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="431"/>
        <source>Name</source>
        <translation>Adı</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="433"/>
        <source>Date</source>
        <translation>Tarih</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="435"/>
        <source>Distance</source>
        <translation>Mesafe</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="437"/>
        <source>Time</source>
        <translation>Zaman</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="439"/>
        <source>Moving time</source>
        <translation>Hareket zamanı</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="441"/>
        <source>Item count (&gt;1)</source>
        <translation>Eşya sayısı (&gt;1)</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="456"/>
        <source>Separate graph page</source>
        <translation>Ayrı grafik sayfası</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="466"/>
        <source>Print mode</source>
        <translation>Yazdırma modu</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="467"/>
        <source>Header</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="475"/>
        <source>Use OpenGL</source>
        <translation>OpenGL Kullan</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="478"/>
        <source>Enable HTTP/2</source>
        <translation>HTTP/2 etkinleştir</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="485"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="495"/>
        <source>Image cache size:</source>
        <translation>Görüntü önbellek boyutu:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="496"/>
        <source>Connection timeout:</source>
        <translation>Bağlantı zaman aşımı:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="512"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="537"/>
        <source>System</source>
        <translation>Sistem</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="530"/>
        <source>Appearance</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="532"/>
        <source>Maps</source>
        <translation>Haritalar</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="533"/>
        <source>Data</source>
        <translation>Veri</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="535"/>
        <source>Print &amp; Export</source>
        <translation>Yazdır &amp; Dışa ver</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="564"/>
        <source>Options</source>
        <translation>Seçenekler</translation>
    </message>
</context>
<context>
    <name>PowerGraph</name>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="11"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="12"/>
        <location filename="../src/GUI/powergraph.h" line="13"/>
        <source>Power</source>
        <translation>Güç</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
</context>
<context>
    <name>PowerGraphItem</name>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="29"/>
        <location filename="../src/GUI/powergraphitem.cpp" line="31"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
</context>
<context>
    <name>RouteItem</name>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="15"/>
        <source>Name</source>
        <translation>Adı</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="17"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="18"/>
        <source>Distance</source>
        <translation>Mesafe</translation>
    </message>
</context>
<context>
    <name>ScaleItem</name>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="108"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="109"/>
        <location filename="../src/GUI/scaleitem.cpp" line="112"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="111"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="114"/>
        <source>km</source>
        <translation>Km</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="115"/>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>SpeedGraph</name>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="16"/>
        <location filename="../src/GUI/speedgraph.h" line="14"/>
        <source>Speed</source>
        <translation>Hız</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="26"/>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/mi</source>
        <translation>min/mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/nmi</source>
        <translation>min/nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="29"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="31"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="33"/>
        <source>Pace</source>
        <translation>Hız</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="96"/>
        <source>kn</source>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="99"/>
        <source>mi/h</source>
        <translation>mi/h</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="102"/>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
</context>
<context>
    <name>SpeedGraphItem</name>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="31"/>
        <source>mi/h</source>
        <translation>mi/h</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>kn</source>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="35"/>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/mi</source>
        <translation>min/mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/nmi</source>
        <translation>min/nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="39"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="41"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="43"/>
        <source>Pace</source>
        <translation>Hız</translation>
    </message>
</context>
<context>
    <name>TemperatureGraph</name>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="12"/>
        <location filename="../src/GUI/temperaturegraph.h" line="13"/>
        <source>Temperature</source>
        <translation>Sıcaklık</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="24"/>
        <source>Minimum</source>
        <translation>Asgari</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="26"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="85"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="89"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>TemperatureGraphItem</name>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="36"/>
        <source>Average</source>
        <translation>Ortalama</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="38"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="40"/>
        <source>Minimum</source>
        <translation>Asgari</translation>
    </message>
</context>
<context>
    <name>TrackItem</name>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="13"/>
        <source>Name</source>
        <translation>Adı</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="15"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="16"/>
        <source>Distance</source>
        <translation>Mesafe</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="18"/>
        <source>Total time</source>
        <translation>Toplam süre</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="20"/>
        <source>Moving time</source>
        <translation>Hareket zamanı</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="22"/>
        <source>Date</source>
        <translation>Tarih</translation>
    </message>
</context>
<context>
    <name>WaypointItem</name>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="18"/>
        <source>Name</source>
        <translation>Adı</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="19"/>
        <source>Coordinates</source>
        <translation>Koordinatlar</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="22"/>
        <source>Elevation</source>
        <translation>Rakım</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="25"/>
        <source>Date</source>
        <translation>Tarih</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="28"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
</context>
</TS>
