<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da_DK">
<context>
    <name>CadenceGraph</name>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="11"/>
        <source>rpm</source>
        <translation>o/min</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="12"/>
        <location filename="../src/GUI/cadencegraph.h" line="13"/>
        <source>Cadence</source>
        <translation>Kadence</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
</context>
<context>
    <name>CadenceGraphItem</name>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="29"/>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="31"/>
        <source>rpm</source>
        <translation>o/min</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
</context>
<context>
    <name>Data</name>
    <message>
        <location filename="../src/data/data.cpp" line="116"/>
        <source>Supported files</source>
        <translation>Understøttede filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>CSV files</source>
        <translation>CSV-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>FIT files</source>
        <translation>FIT-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>GPX files</source>
        <translation>GPX-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>IGC files</source>
        <translation>IGC-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>KML files</source>
        <translation>KML-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>LOC files</source>
        <translation>LOC-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="121"/>
        <source>NMEA files</source>
        <translation>NMEA-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="122"/>
        <source>OziExplorer files</source>
        <translation>OziExplorer-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>SLF files</source>
        <translation>SLF-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>TCX files</source>
        <translation>TCX-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="124"/>
        <source>All files</source>
        <translation>Alle filer</translation>
    </message>
</context>
<context>
    <name>ElevationGraph</name>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="48"/>
        <location filename="../src/GUI/elevationgraph.h" line="13"/>
        <source>Elevation</source>
        <translation>Højde</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="59"/>
        <source>Ascent</source>
        <translation>Stigning</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="61"/>
        <source>Descent</source>
        <translation>Fald</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="63"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="65"/>
        <source>Minimum</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="127"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="130"/>
        <source>ft</source>
        <translation>fod</translation>
    </message>
</context>
<context>
    <name>ElevationGraphItem</name>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>ft</source>
        <translation>fod</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="37"/>
        <source>Ascent</source>
        <translation>Stigning</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="39"/>
        <source>Descent</source>
        <translation>Fald</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="41"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="43"/>
        <source>Minimum</source>
        <translation>Min</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>PDF files</source>
        <translation>PDF-filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>All files</source>
        <translation>Alle filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="52"/>
        <source>Portrait</source>
        <translation>Stående</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="53"/>
        <source>Landscape</source>
        <translation>Liggende</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>in</source>
        <translation type="unfinished">tomme</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="94"/>
        <source>Page Setup</source>
        <translation>Sideopsætning</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="97"/>
        <source>Page size:</source>
        <translation>Sidestørrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="98"/>
        <source>Resolution:</source>
        <translation>Opløsning:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="99"/>
        <source>Orientation:</source>
        <translation>Orientering:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="100"/>
        <source>Margins:</source>
        <translation>Margener:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="106"/>
        <source>File:</source>
        <translation>Fil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="113"/>
        <source>Output file</source>
        <translation>Uddatafil</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="120"/>
        <source>Export</source>
        <translation>Eksporter</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="135"/>
        <source>Export to PDF</source>
        <translation>Eksporter til PDF</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <source>No output file selected.</source>
        <translation>Ingen uddatafil valgt.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <source>%1 is a directory.</source>
        <translation>%1 er en mappe.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>%1 is not writable.</source>
        <translation>%1 er skrivebeskyttet.</translation>
    </message>
</context>
<context>
    <name>FileSelectWidget</name>
    <message>
        <location filename="../src/GUI/fileselectwidget.cpp" line="38"/>
        <source>Select file</source>
        <translation>Vælg fil</translation>
    </message>
</context>
<context>
    <name>Format</name>
    <message>
        <location filename="../src/GUI/format.cpp" line="54"/>
        <location filename="../src/GUI/format.cpp" line="61"/>
        <location filename="../src/GUI/format.cpp" line="84"/>
        <source>ft</source>
        <translation>fod</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="57"/>
        <source>mi</source>
        <translation>mil</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="64"/>
        <source>nmi</source>
        <translation>sømil</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="68"/>
        <location filename="../src/GUI/format.cpp" line="81"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="71"/>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>GUI</name>
    <message>
        <location filename="../src/GUI/gui.cpp" line="199"/>
        <source>Quit</source>
        <translation>Afslut</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="206"/>
        <location filename="../src/GUI/gui.cpp" line="693"/>
        <location filename="../src/GUI/gui.cpp" line="694"/>
        <source>Paths</source>
        <translation>Stier</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="209"/>
        <location filename="../src/GUI/gui.cpp" line="659"/>
        <location filename="../src/GUI/gui.cpp" line="660"/>
        <source>Keyboard controls</source>
        <translation type="unfinished">Tastaturgenveje</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="212"/>
        <location filename="../src/GUI/gui.cpp" line="637"/>
        <source>About GPXSee</source>
        <translation>Om GPXSee</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="217"/>
        <source>Open...</source>
        <translation>Åbn...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="222"/>
        <source>Print...</source>
        <translation>Udskriv…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="229"/>
        <source>Export to PDF...</source>
        <translation>Eksporter til PDF…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="235"/>
        <source>Close</source>
        <translation>Luk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="241"/>
        <source>Reload</source>
        <translation>Genindlæs</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="248"/>
        <source>Statistics...</source>
        <translation>Statistik...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="256"/>
        <source>Load POI file...</source>
        <translation>Indlæs IP-fil...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="260"/>
        <source>Close POI files</source>
        <translation>Luk IP-filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="264"/>
        <source>Overlap POIs</source>
        <translation>Overlap IP&apos;er</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="269"/>
        <source>Show POI labels</source>
        <translation>Vis IP-etiketter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="274"/>
        <source>Show POIs</source>
        <translation type="unfinished">Vis IP&apos;er</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="284"/>
        <source>Show map</source>
        <translation>Vis kort</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="292"/>
        <source>Load map...</source>
        <translation>Indlæs kort...</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="296"/>
        <source>Clear tile cache</source>
        <translation type="unfinished">Ryd kortbrikmellemlager (cache)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="301"/>
        <location filename="../src/GUI/gui.cpp" line="306"/>
        <location filename="../src/GUI/gui.cpp" line="676"/>
        <source>Next map</source>
        <translation>Næste kort</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="317"/>
        <source>Show tracks</source>
        <translation>Vis spor</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="322"/>
        <source>Show routes</source>
        <translation>Vis ruter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="327"/>
        <source>Show waypoints</source>
        <translation>Vis rutepunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="332"/>
        <source>Waypoint labels</source>
        <translation>Rutepunktsetiketter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="337"/>
        <source>Route waypoints</source>
        <translation type="unfinished">Vej rutepunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="344"/>
        <source>Show graphs</source>
        <translation>Vis grafer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="354"/>
        <location filename="../src/GUI/gui.cpp" line="1004"/>
        <location filename="../src/GUI/gui.cpp" line="1069"/>
        <source>Distance</source>
        <translation>Afstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="361"/>
        <location filename="../src/GUI/gui.cpp" line="527"/>
        <location filename="../src/GUI/gui.cpp" line="1007"/>
        <location filename="../src/GUI/gui.cpp" line="1071"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="368"/>
        <source>Show grid</source>
        <translation>Vis gitter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="373"/>
        <source>Show slider info</source>
        <translation type="unfinished">Vis skyder info</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="380"/>
        <source>Show toolbars</source>
        <translation>Vis værktøjslinjer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="387"/>
        <source>Total time</source>
        <translation>Samlet tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="393"/>
        <location filename="../src/GUI/gui.cpp" line="1009"/>
        <location filename="../src/GUI/gui.cpp" line="1073"/>
        <source>Moving time</source>
        <translation>Tid i bevægelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="401"/>
        <source>Metric</source>
        <translation>Metrisk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="407"/>
        <source>Imperial</source>
        <translation>Imperial</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="413"/>
        <source>Nautical</source>
        <translation>Nautisk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="421"/>
        <source>Decimal degrees (DD)</source>
        <translation>Decimalgrader (DD)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="427"/>
        <source>Degrees and decimal minutes (DMM)</source>
        <translation>Grader og decimalminutter (DMM)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="434"/>
        <source>Degrees, minutes, seconds (DMS)</source>
        <translation>Grader, minutter, sekunder (DMS)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="440"/>
        <source>Fullscreen mode</source>
        <translation>Fuldskærmstilstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="447"/>
        <source>Options...</source>
        <translation>Indstillinger…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="453"/>
        <source>Next</source>
        <translation>Næste</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="457"/>
        <source>Previous</source>
        <translation>Forrige</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="461"/>
        <source>Last</source>
        <translation>Sidste</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="465"/>
        <source>First</source>
        <translation>Første</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="473"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="488"/>
        <source>&amp;Map</source>
        <translation>&amp;Kort</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="496"/>
        <source>&amp;Graph</source>
        <translation>&amp;Graf</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="505"/>
        <source>&amp;POI</source>
        <translation>&amp;IP</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="506"/>
        <source>POI files</source>
        <translation>IP- filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="517"/>
        <source>&amp;Data</source>
        <translation>&amp;Data</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="518"/>
        <source>Display</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="526"/>
        <source>&amp;Settings</source>
        <translation>&amp;Indstillinger</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="530"/>
        <source>Units</source>
        <translation>Enheder</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="534"/>
        <source>Coordinates format</source>
        <translation>koordinatformat</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="544"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjælp</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="560"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="567"/>
        <source>Show</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="573"/>
        <source>Navigation</source>
        <translation>Navigation</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="638"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="642"/>
        <source>GPXSee is distributed under the terms of the GNU General Public License version 3. For more info about GPXSee visit the project homepage at %1.</source>
        <translation>GPXSee er distribueret under betingelserne i GNU General Public License version 3. For mere info om GPXSee besøg projektets hjemmeside på %1.</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="663"/>
        <source>Next file</source>
        <translation>Næste fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="664"/>
        <source>Previous file</source>
        <translation>Forrige fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="666"/>
        <source>First file</source>
        <translation>Første fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="668"/>
        <source>Last file</source>
        <translation>Sidste fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="669"/>
        <source>Append file</source>
        <translation>Vedhæft fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="670"/>
        <source>Next/Previous</source>
        <translation>Næste/Forrige</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="672"/>
        <source>Toggle graph type</source>
        <translation>Skift graftype</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="674"/>
        <source>Toggle time type</source>
        <translation>Skift tidstype</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="678"/>
        <source>Previous map</source>
        <translation>Forrige kort</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="679"/>
        <source>Zoom in</source>
        <translation>Zoom ind</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="681"/>
        <source>Zoom out</source>
        <translation>Zoom ud</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="683"/>
        <source>Digital zoom</source>
        <translation>Digital zoom</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="684"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="703"/>
        <source>Tile cache directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="697"/>
        <source>Map directory:</source>
        <translation>Kortmappe:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="699"/>
        <source>POI directory:</source>
        <translation>IP-mappe:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="701"/>
        <source>GCS/PCS directory:</source>
        <translation>GCS/PSC-mappe:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="712"/>
        <source>Open file</source>
        <translation>Åbn fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="806"/>
        <source>Error loading data file:</source>
        <translation>Fejl ved indlæsning af data-fil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="809"/>
        <location filename="../src/GUI/gui.cpp" line="844"/>
        <source>Line: %1</source>
        <translation>Linje: %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="817"/>
        <source>Open POI file</source>
        <translation>Åbn IP-fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="841"/>
        <source>Error loading POI file:</source>
        <translation>Fejl ved indlæsning af IP-fil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="981"/>
        <location filename="../src/GUI/gui.cpp" line="1049"/>
        <source>Tracks</source>
        <translation>Spor</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="984"/>
        <location filename="../src/GUI/gui.cpp" line="1051"/>
        <source>Routes</source>
        <translation>Ruter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="987"/>
        <location filename="../src/GUI/gui.cpp" line="1053"/>
        <source>Waypoints</source>
        <translation>Rutepunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="993"/>
        <location filename="../src/GUI/gui.cpp" line="997"/>
        <location filename="../src/GUI/gui.cpp" line="1059"/>
        <location filename="../src/GUI/gui.cpp" line="1062"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1030"/>
        <location filename="../src/GUI/gui.cpp" line="1031"/>
        <source>Statistics</source>
        <translation>Statistikker</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1045"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1272"/>
        <source>Open map file</source>
        <translation>Åbn kort-fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1295"/>
        <source>Error loading map:</source>
        <translation>Fejl ved indlæsning af kort:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1306"/>
        <source>No files loaded</source>
        <translation>Ingen filer indlæst</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/GUI/gui.cpp" line="1310"/>
        <source>%n files</source>
        <translation>
            <numerusform>%n fil</numerusform>
            <numerusform>%n filer</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>GearRatioGraph</name>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="12"/>
        <location filename="../src/GUI/gearratiograph.h" line="14"/>
        <source>Gear ratio</source>
        <translation type="unfinished">Udvekslingsforhold</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="22"/>
        <source>Most used</source>
        <translation>Mest anvendte</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="24"/>
        <source>Minimum</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="26"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
</context>
<context>
    <name>GearRatioGraphItem</name>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="44"/>
        <source>Minimum</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="45"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="46"/>
        <source>Most used</source>
        <translation>Mest brugt</translation>
    </message>
</context>
<context>
    <name>GraphView</name>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="46"/>
        <source>Data not available</source>
        <translation>Data er ikke tilgængelig</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="66"/>
        <location filename="../src/GUI/graphview.cpp" line="181"/>
        <source>Distance</source>
        <translation>Afstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="111"/>
        <location filename="../src/GUI/graphview.cpp" line="119"/>
        <source>ft</source>
        <translation>fod</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="114"/>
        <source>mi</source>
        <translation>mil</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="122"/>
        <source>nmi</source>
        <translation>sømil</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="127"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="130"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="136"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="139"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="142"/>
        <source>h</source>
        <translation>t</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="183"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
</context>
<context>
    <name>HeartRateGraph</name>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="11"/>
        <source>bpm</source>
        <translation type="unfinished">puls</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="12"/>
        <location filename="../src/GUI/heartrategraph.h" line="13"/>
        <source>Heart rate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
</context>
<context>
    <name>HeartRateGraphItem</name>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="29"/>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="31"/>
        <source>bpm</source>
        <translation type="unfinished">puls</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
</context>
<context>
    <name>MapList</name>
    <message>
        <location filename="../src/map/maplist.cpp" line="117"/>
        <source>Supported files</source>
        <translation>Understøttede filer</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="119"/>
        <source>MBTiles maps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="120"/>
        <source>Garmin JNX maps</source>
        <translation>Garmin JNX-kort</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="121"/>
        <source>OziExplorer maps</source>
        <translation>OziExplorer-kort</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="122"/>
        <source>TrekBuddy maps/atlases</source>
        <translation>TrekBuddy kort/atlas</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="123"/>
        <source>GeoTIFF images</source>
        <translation>GeoTIFF billeder</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="124"/>
        <source>Online map sources</source>
        <translation type="unfinished">Online-kort kilder</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="39"/>
        <source>Always show the map</source>
        <translation>Vis altid kortet</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="42"/>
        <source>Show the map even when no files are loaded.</source>
        <translation>Vis kortet, selv når ingen filer er indlæst.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="45"/>
        <source>High-resolution</source>
        <translation>Højopløsning</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="46"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="51"/>
        <source>Non-HiDPI maps are loaded as HiDPI maps. The map is sharp but map objects are small/hard to read.</source>
        <translation>Ikke HiDPI kort er indlæst som HiDPI kort. Kortet er skarpt, men kortobjekter er små/svære at læse.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="53"/>
        <source>Non-HiDPI maps are loaded such as they are. Map objects have the expected size but the map is blurry.</source>
        <translation>Ikke HiDPI kort er indlæst som de er. Kortobjekter har den forventede størrelse, men kortet er sløret.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="86"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="88"/>
        <source>HiDPI display mode</source>
        <translation>HiDPI skærmtilstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="105"/>
        <source>Base color:</source>
        <translation>Grundfarve:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="106"/>
        <source>Palette shift:</source>
        <translation>Paletteskift:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="108"/>
        <source>Colors</source>
        <translation>Farver</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="119"/>
        <source>Track width:</source>
        <translation>Sporvidde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="120"/>
        <source>Track style:</source>
        <translation>Sporstil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="122"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="138"/>
        <source>Width:</source>
        <translation>Bredde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="123"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="139"/>
        <source>Style:</source>
        <translation>Stil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="124"/>
        <source>Tracks</source>
        <translation>Spor</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="135"/>
        <source>Route width:</source>
        <translation>Rutebredde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="136"/>
        <source>Route style:</source>
        <translation>Rutestil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="140"/>
        <source>Routes</source>
        <translation>Ruter</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="144"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="226"/>
        <source>Use anti-aliasing</source>
        <translation>Brug anti-aliasing</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="176"/>
        <source>Waypoint color:</source>
        <translation>Rutepunktfarve:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="177"/>
        <source>Waypoint size:</source>
        <translation>Rutepunktstørrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="179"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="195"/>
        <source>Color:</source>
        <translation>Farve:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="180"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="196"/>
        <source>Size:</source>
        <translation>Størrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="181"/>
        <source>Waypoints</source>
        <translation>Rutepunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="192"/>
        <source>POI color:</source>
        <translation>IP farve:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="193"/>
        <source>POI size:</source>
        <translation>IP størrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="197"/>
        <source>POIs</source>
        <translation type="unfinished">Interessepunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="223"/>
        <source>Line width:</source>
        <translation>Linjebredde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="224"/>
        <source>Slider color:</source>
        <translation type="unfinished">Skyderfarve:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="246"/>
        <source>Background color:</source>
        <translation>Baggrundsfarve:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="247"/>
        <source>Map opacity:</source>
        <translation type="unfinished">Gennemsigtighed for kort:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="257"/>
        <source>Paths</source>
        <translation>Stier</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="258"/>
        <source>Points</source>
        <translation>Punkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="259"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="468"/>
        <source>Graphs</source>
        <translation>Grafer</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="260"/>
        <source>Map</source>
        <translation>Kort</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="267"/>
        <source>Moving average window size</source>
        <translation type="unfinished">Glidende gennemsnitlig vinduesstørrelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="286"/>
        <source>Elevation:</source>
        <translation>Højde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="287"/>
        <source>Speed:</source>
        <translation>Hastighed:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="288"/>
        <source>Heart rate:</source>
        <translation>Puls:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="289"/>
        <source>Cadence:</source>
        <translation>Kadence:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="290"/>
        <source>Power:</source>
        <translation>Effekt:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="292"/>
        <source>Smoothing</source>
        <translation>Udjævning</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="296"/>
        <source>Eliminate GPS outliers</source>
        <translation>Eliminer GPS-ekstremværdier</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="302"/>
        <source>Outlier elimination</source>
        <translation>Ekstremværdieliminering</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="309"/>
        <source>Smoothing:</source>
        <translation>Udjævning:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="327"/>
        <source>mi/h</source>
        <translation>mil/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="330"/>
        <source>kn</source>
        <translation type="unfinished">knob</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="333"/>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="337"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="491"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="341"/>
        <source>Minimal speed:</source>
        <translation>Mindste hastighed:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="342"/>
        <source>Minimal duration:</source>
        <translation>Minidste varighed:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="348"/>
        <source>Computed from distance/time</source>
        <translation>Beregnet ud fra afstanden/tiden</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="349"/>
        <source>Recorded by device</source>
        <translation>Indspillet af enhed</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="364"/>
        <source>Filtering</source>
        <translation>Filtrering</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="365"/>
        <source>Pause detection</source>
        <translation type="unfinished">Pause genkendelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="366"/>
        <source>Speed</source>
        <translation>Hastighed</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="378"/>
        <source>mi</source>
        <translation>mil</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="381"/>
        <source>nmi</source>
        <translation>sømil</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="384"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="388"/>
        <source>POI radius:</source>
        <translation>IP-radius:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="394"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="534"/>
        <source>POI</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="401"/>
        <source>WYSIWYG</source>
        <translation type="unfinished">WYSIWYG</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="402"/>
        <source>High-Resolution</source>
        <translation>Højopløsning</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="407"/>
        <source>The printed area is approximately the display area. The map zoom level does not change.</source>
        <translation type="unfinished">Det udskrevne område er ca. det samme som visningsområdet. Kortes zoomniveau ændres ikke.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="409"/>
        <source>The zoom level will be changed so that the whole content (tracks/waypoints) fits to the printed area and the map resolution is as close as possible to the print resolution.</source>
        <translation type="unfinished">Zoom-niveauet vil blive ændret, således at hele indholdet (spor/rutepunkter) passer til udskriftsområdet og kortopløsning er så tæt som muligt på udskriftsopløsningen.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="431"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="433"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="435"/>
        <source>Distance</source>
        <translation>Afstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="437"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="439"/>
        <source>Moving time</source>
        <translation>Tid i bevægelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="441"/>
        <source>Item count (&gt;1)</source>
        <translation type="unfinished">Elementantal (&gt;1)</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="456"/>
        <source>Separate graph page</source>
        <translation>Separat grafside</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="466"/>
        <source>Print mode</source>
        <translation type="unfinished">Udskrivningstilstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="467"/>
        <source>Header</source>
        <translation type="unfinished">Toptekst</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="475"/>
        <source>Use OpenGL</source>
        <translation>Brug OpenGL</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="478"/>
        <source>Enable HTTP/2</source>
        <translation>Aktiver HTTP/2</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="485"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="495"/>
        <source>Image cache size:</source>
        <translation type="unfinished">Billed cachestørrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="496"/>
        <source>Connection timeout:</source>
        <translation>Timeout for forbindelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="512"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="537"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="530"/>
        <source>Appearance</source>
        <translation>Udseende</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="532"/>
        <source>Maps</source>
        <translation>Kort</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="533"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="535"/>
        <source>Print &amp; Export</source>
        <translation>Udskriv &amp; eksport</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="564"/>
        <source>Options</source>
        <translation>Indstillinger</translation>
    </message>
</context>
<context>
    <name>PowerGraph</name>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="11"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="12"/>
        <location filename="../src/GUI/powergraph.h" line="13"/>
        <source>Power</source>
        <translation>Effekt</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
</context>
<context>
    <name>PowerGraphItem</name>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="29"/>
        <location filename="../src/GUI/powergraphitem.cpp" line="31"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
</context>
<context>
    <name>RouteItem</name>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="15"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="17"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="18"/>
        <source>Distance</source>
        <translation>Afstand</translation>
    </message>
</context>
<context>
    <name>ScaleItem</name>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="108"/>
        <source>mi</source>
        <translation>mil</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="109"/>
        <location filename="../src/GUI/scaleitem.cpp" line="112"/>
        <source>ft</source>
        <translation>fod</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="111"/>
        <source>nmi</source>
        <translation>sømil</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="114"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="115"/>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>SpeedGraph</name>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="16"/>
        <location filename="../src/GUI/speedgraph.h" line="14"/>
        <source>Speed</source>
        <translation>Hastighed</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="26"/>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/mi</source>
        <translation type="unfinished">min/mil</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/nmi</source>
        <translation type="unfinished">min/sømil</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="29"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="31"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="33"/>
        <source>Pace</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="96"/>
        <source>kn</source>
        <translation type="unfinished">knob</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="99"/>
        <source>mi/h</source>
        <translation>mil/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="102"/>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
</context>
<context>
    <name>SpeedGraphItem</name>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="31"/>
        <source>mi/h</source>
        <translation>mil/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>kn</source>
        <translation type="unfinished">knob</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="35"/>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/mi</source>
        <translation type="unfinished">min/mil</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/nmi</source>
        <translation type="unfinished">min/sømil</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="39"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="41"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="43"/>
        <source>Pace</source>
        <translation>Tempo</translation>
    </message>
</context>
<context>
    <name>TemperatureGraph</name>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="12"/>
        <location filename="../src/GUI/temperaturegraph.h" line="13"/>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="24"/>
        <source>Minimum</source>
        <translation type="unfinished">Min</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="26"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="85"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="89"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>TemperatureGraphItem</name>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="36"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="38"/>
        <source>Maximum</source>
        <translation type="unfinished">Max</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="40"/>
        <source>Minimum</source>
        <translation type="unfinished">Min</translation>
    </message>
</context>
<context>
    <name>TrackItem</name>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="13"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="15"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="16"/>
        <source>Distance</source>
        <translation>Afstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="18"/>
        <source>Total time</source>
        <translation>Samlet tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="20"/>
        <source>Moving time</source>
        <translation>Tid i bevægelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="22"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
</context>
<context>
    <name>WaypointItem</name>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="18"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="19"/>
        <source>Coordinates</source>
        <translation>Koordinater</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="22"/>
        <source>Elevation</source>
        <translation>Højde</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="25"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="28"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
</context>
</TS>
