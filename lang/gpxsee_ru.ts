<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>CadenceGraph</name>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="11"/>
        <source>rpm</source>
        <translation>об/мин</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="12"/>
        <location filename="../src/GUI/cadencegraph.h" line="13"/>
        <source>Cadence</source>
        <translation>Каденс</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
</context>
<context>
    <name>CadenceGraphItem</name>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="29"/>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="31"/>
        <source>rpm</source>
        <translation>об/мин</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
</context>
<context>
    <name>Data</name>
    <message>
        <location filename="../src/data/data.cpp" line="116"/>
        <source>Supported files</source>
        <translation>Все поддерживаемые файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>CSV files</source>
        <translation>CSV файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>FIT files</source>
        <translation>FIT файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>GPX files</source>
        <translation>GPX файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>IGC files</source>
        <translation>IGC файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>KML files</source>
        <translation>KML файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>LOC files</source>
        <translation>LOC файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="121"/>
        <source>NMEA files</source>
        <translation>NMEA файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="122"/>
        <source>OziExplorer files</source>
        <translation>OziExplorer файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>TCX files</source>
        <translation>TCX файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>SLF files</source>
        <translation>SLF файлы</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="124"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
</context>
<context>
    <name>ElevationGraph</name>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="48"/>
        <location filename="../src/GUI/elevationgraph.h" line="13"/>
        <source>Elevation</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="127"/>
        <source>m</source>
        <translation>м</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="59"/>
        <source>Ascent</source>
        <translation>Подъем</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="61"/>
        <source>Descent</source>
        <translation>Спуск</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="65"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="130"/>
        <source>ft</source>
        <translation>фт</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="63"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
</context>
<context>
    <name>ElevationGraphItem</name>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>m</source>
        <translation>м</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>ft</source>
        <translation>фт</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="37"/>
        <source>Ascent</source>
        <translation>Подъем</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="39"/>
        <source>Descent</source>
        <translation>Спуск</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="41"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="43"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="135"/>
        <source>Export to PDF</source>
        <translation>Экспорт в PDF</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="52"/>
        <source>Portrait</source>
        <translation>Портретная</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="53"/>
        <source>Landscape</source>
        <translation>Ландшафтная</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="97"/>
        <source>Page size:</source>
        <translation>Размер страницы:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="99"/>
        <source>Orientation:</source>
        <translation>Ориентация:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="94"/>
        <source>Page Setup</source>
        <translation>Параметры страницы</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>PDF files</source>
        <translation>PDF файлы</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>in</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>mm</source>
        <translation>мм</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="98"/>
        <source>Resolution:</source>
        <translation>Разрешение:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="100"/>
        <source>Margins:</source>
        <translation>Поля:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="106"/>
        <source>File:</source>
        <translation>Файл:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="113"/>
        <source>Output file</source>
        <translation>Файл вывода</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="120"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <source>No output file selected.</source>
        <translation>Не выбран файл вывода.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <source>%1 is a directory.</source>
        <translation>Файл %1 - это директория.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>%1 is not writable.</source>
        <translation>%1 не доступен для записи.</translation>
    </message>
</context>
<context>
    <name>FileSelectWidget</name>
    <message>
        <location filename="../src/GUI/fileselectwidget.cpp" line="38"/>
        <source>Select file</source>
        <translation>Выбрать файл</translation>
    </message>
</context>
<context>
    <name>Format</name>
    <message>
        <location filename="../src/GUI/format.cpp" line="54"/>
        <location filename="../src/GUI/format.cpp" line="61"/>
        <location filename="../src/GUI/format.cpp" line="84"/>
        <source>ft</source>
        <translation>фт</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="57"/>
        <source>mi</source>
        <translation>мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="64"/>
        <source>nmi</source>
        <translation>мор. мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="68"/>
        <location filename="../src/GUI/format.cpp" line="81"/>
        <source>m</source>
        <translation>м</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="71"/>
        <source>km</source>
        <translation>км</translation>
    </message>
</context>
<context>
    <name>GUI</name>
    <message>
        <location filename="../src/GUI/gui.cpp" line="712"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="817"/>
        <source>Open POI file</source>
        <translation>Открыть файл с точками POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="199"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="209"/>
        <location filename="../src/GUI/gui.cpp" line="659"/>
        <location filename="../src/GUI/gui.cpp" line="660"/>
        <source>Keyboard controls</source>
        <translation>Управление с помощью клавиатуры</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="235"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="241"/>
        <source>Reload</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="567"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="560"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="260"/>
        <source>Close POI files</source>
        <translation>Закрыть файлы с точками POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="264"/>
        <source>Overlap POIs</source>
        <translation>Перекрывать точки POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="269"/>
        <source>Show POI labels</source>
        <translation>Показать подписи к точкам POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="274"/>
        <source>Show POIs</source>
        <translation>Показать точки POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="284"/>
        <source>Show map</source>
        <translation>Показать карту</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="296"/>
        <source>Clear tile cache</source>
        <translation>Очистить кэш</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="217"/>
        <source>Open...</source>
        <translation>Открыть…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="206"/>
        <location filename="../src/GUI/gui.cpp" line="693"/>
        <location filename="../src/GUI/gui.cpp" line="694"/>
        <source>Paths</source>
        <translation>Пути</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="248"/>
        <source>Statistics...</source>
        <translation>Статистика…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="256"/>
        <source>Load POI file...</source>
        <translation>Загрузить файл с точками POI…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="292"/>
        <source>Load map...</source>
        <translation>Загрузить карту…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="301"/>
        <location filename="../src/GUI/gui.cpp" line="306"/>
        <location filename="../src/GUI/gui.cpp" line="676"/>
        <source>Next map</source>
        <translation>Следующая карта</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="317"/>
        <source>Show tracks</source>
        <translation>Показать треки</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="322"/>
        <source>Show routes</source>
        <translation>Показать маршруты</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="327"/>
        <source>Show waypoints</source>
        <translation>Показать точки</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="332"/>
        <source>Waypoint labels</source>
        <translation>Подписи точек</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="344"/>
        <source>Show graphs</source>
        <translation>Показать графики</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="368"/>
        <source>Show grid</source>
        <translation>Показать сетку</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="373"/>
        <source>Show slider info</source>
        <translation>Показать значение на слайдере</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="380"/>
        <source>Show toolbars</source>
        <translation>Показывать панели инструментов</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="387"/>
        <source>Total time</source>
        <translation>Общее время</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="393"/>
        <location filename="../src/GUI/gui.cpp" line="1009"/>
        <location filename="../src/GUI/gui.cpp" line="1073"/>
        <source>Moving time</source>
        <translation>Время движения</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="401"/>
        <source>Metric</source>
        <translation>Метрические</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="407"/>
        <source>Imperial</source>
        <translation>Британские</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="413"/>
        <source>Nautical</source>
        <translation>Морские</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="421"/>
        <source>Decimal degrees (DD)</source>
        <translation>Десятичные градусы (DD)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="427"/>
        <source>Degrees and decimal minutes (DMM)</source>
        <translation>Градусы, десятичные минуты (DMM)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="434"/>
        <source>Degrees, minutes, seconds (DMS)</source>
        <translation>Градусы, минуты, секунды (DMS)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="440"/>
        <source>Fullscreen mode</source>
        <translation>Полноэкранный режим</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="447"/>
        <source>Options...</source>
        <translation>Параметры…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="453"/>
        <source>Next</source>
        <translation>Следующий</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="457"/>
        <source>Previous</source>
        <translation>Предыдущий</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="461"/>
        <source>Last</source>
        <translation>Последний</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="465"/>
        <source>First</source>
        <translation>Первый</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="506"/>
        <source>POI files</source>
        <translation>Файлы с точками POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="518"/>
        <source>Display</source>
        <translation>Отображать</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="530"/>
        <source>Units</source>
        <translation>Единицы</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="534"/>
        <source>Coordinates format</source>
        <translation>Формат координат</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="638"/>
        <source>Version %1</source>
        <translation>Версия %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="642"/>
        <source>GPXSee is distributed under the terms of the GNU General Public License version 3. For more info about GPXSee visit the project homepage at %1.</source>
        <translation>GPXSee распространяется в соответствиями с условиями версии 3 Стандартной Общественной Лицензии GNU. Для получения дополнительной информации о GPXSee посетите страницу проекта %1.</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="669"/>
        <source>Append file</source>
        <translation>Добавить файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="670"/>
        <source>Next/Previous</source>
        <translation>Следующий/предыдущий</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="672"/>
        <source>Toggle graph type</source>
        <translation>Переключить тип графика</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="674"/>
        <source>Toggle time type</source>
        <translation>Переключить тип времени</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="678"/>
        <source>Previous map</source>
        <translation>Предыдущая карта</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="679"/>
        <source>Zoom in</source>
        <translation>Увеличить</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="681"/>
        <source>Zoom out</source>
        <translation>Уменьшить</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="683"/>
        <source>Digital zoom</source>
        <translation>Цифровой зум</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="684"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="701"/>
        <source>GCS/PCS directory:</source>
        <translation>Директория с GCS/PCS:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1030"/>
        <location filename="../src/GUI/gui.cpp" line="1031"/>
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1272"/>
        <source>Open map file</source>
        <translation>Открыть файл карты</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1306"/>
        <source>No files loaded</source>
        <translation>Нет загруженных файлов</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="993"/>
        <location filename="../src/GUI/gui.cpp" line="997"/>
        <location filename="../src/GUI/gui.cpp" line="1059"/>
        <location filename="../src/GUI/gui.cpp" line="1062"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="473"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="488"/>
        <source>&amp;Map</source>
        <translation>&amp;Карты</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="496"/>
        <source>&amp;Graph</source>
        <translation>&amp;График</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="505"/>
        <source>&amp;POI</source>
        <translation>&amp;Точки POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="517"/>
        <source>&amp;Data</source>
        <translation>&amp;Данные</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="526"/>
        <source>&amp;Settings</source>
        <translation>&amp;Параметры</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="544"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="697"/>
        <source>Map directory:</source>
        <translation>Директория с картами:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="699"/>
        <source>POI directory:</source>
        <translation>Директория с POI:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="703"/>
        <source>Tile cache directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="984"/>
        <location filename="../src/GUI/gui.cpp" line="1051"/>
        <source>Routes</source>
        <translation>Маршруты</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1295"/>
        <source>Error loading map:</source>
        <translation>Ошибка загрузки карты:</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/GUI/gui.cpp" line="1310"/>
        <source>%n files</source>
        <translation>
            <numerusform>%n файл</numerusform>
            <numerusform>%n файла</numerusform>
            <numerusform>%n файлов</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="663"/>
        <source>Next file</source>
        <translation>Следующий файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="222"/>
        <source>Print...</source>
        <translation>Печать…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="229"/>
        <source>Export to PDF...</source>
        <translation>Экспорт в PDF…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="987"/>
        <location filename="../src/GUI/gui.cpp" line="1053"/>
        <source>Waypoints</source>
        <translation>Точки</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="664"/>
        <source>Previous file</source>
        <translation>Предыдущий файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="337"/>
        <source>Route waypoints</source>
        <translation>Маршрутные точки</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="666"/>
        <source>First file</source>
        <translation>Первый файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="668"/>
        <source>Last file</source>
        <translation>Последний файл</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="806"/>
        <source>Error loading data file:</source>
        <translation>Ошибка загрузки файла данных:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="809"/>
        <location filename="../src/GUI/gui.cpp" line="844"/>
        <source>Line: %1</source>
        <translation>Строка: %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="841"/>
        <source>Error loading POI file:</source>
        <translation>Ошибка загрузки файла с точками POI:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1045"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="981"/>
        <location filename="../src/GUI/gui.cpp" line="1049"/>
        <source>Tracks</source>
        <translation>Треки</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="212"/>
        <location filename="../src/GUI/gui.cpp" line="637"/>
        <source>About GPXSee</source>
        <translation>О GPXSee</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="573"/>
        <source>Navigation</source>
        <translation>Навигация</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="354"/>
        <location filename="../src/GUI/gui.cpp" line="1004"/>
        <location filename="../src/GUI/gui.cpp" line="1069"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="361"/>
        <location filename="../src/GUI/gui.cpp" line="527"/>
        <location filename="../src/GUI/gui.cpp" line="1007"/>
        <location filename="../src/GUI/gui.cpp" line="1071"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
</context>
<context>
    <name>GearRatioGraph</name>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="12"/>
        <location filename="../src/GUI/gearratiograph.h" line="14"/>
        <source>Gear ratio</source>
        <translation>Передаточное отношение</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="22"/>
        <source>Most used</source>
        <translation>Чаще всего используемое</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="24"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="26"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
</context>
<context>
    <name>GearRatioGraphItem</name>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="44"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="45"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="46"/>
        <source>Most used</source>
        <translation>Чаще всего используемое</translation>
    </message>
</context>
<context>
    <name>GraphView</name>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="127"/>
        <source>m</source>
        <translation>м</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="130"/>
        <source>km</source>
        <translation>км</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="111"/>
        <location filename="../src/GUI/graphview.cpp" line="119"/>
        <source>ft</source>
        <translation>фт</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="46"/>
        <source>Data not available</source>
        <translation>Данные отсутствуют</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="114"/>
        <source>mi</source>
        <translation>мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="122"/>
        <source>nmi</source>
        <translation>мор. мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="136"/>
        <source>s</source>
        <translation>с</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="139"/>
        <source>min</source>
        <translation>мин</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="142"/>
        <source>h</source>
        <translation>ч</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="66"/>
        <location filename="../src/GUI/graphview.cpp" line="181"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="183"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
</context>
<context>
    <name>HeartRateGraph</name>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="12"/>
        <location filename="../src/GUI/heartrategraph.h" line="13"/>
        <source>Heart rate</source>
        <translation>Пульс</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="11"/>
        <source>bpm</source>
        <translation>уд/мин</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
</context>
<context>
    <name>HeartRateGraphItem</name>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="29"/>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="31"/>
        <source>bpm</source>
        <translation>уд/мин</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
</context>
<context>
    <name>MapList</name>
    <message>
        <location filename="../src/map/maplist.cpp" line="117"/>
        <source>Supported files</source>
        <translation>Все поддерживаемые файлы</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="119"/>
        <source>MBTiles maps</source>
        <translation>MBTiles карты</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="120"/>
        <source>Garmin JNX maps</source>
        <translation>Garmin JNX карты</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="121"/>
        <source>OziExplorer maps</source>
        <translation>OziExplorer карты</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="122"/>
        <source>TrekBuddy maps/atlases</source>
        <translation>TrekBuddy карты/атласы</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="123"/>
        <source>GeoTIFF images</source>
        <translation>GeoTIFF изображения</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="124"/>
        <source>Online map sources</source>
        <translation>Источники онлайн карт</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="105"/>
        <source>Base color:</source>
        <translation>Основной цвет:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="106"/>
        <source>Palette shift:</source>
        <translation>Смещение палитры:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="119"/>
        <source>Track width:</source>
        <translation>Толщина трека:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="120"/>
        <source>Track style:</source>
        <translation>Стиль трека:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="124"/>
        <source>Tracks</source>
        <translation>Треки</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="135"/>
        <source>Route width:</source>
        <translation>Толщина маршрута:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="136"/>
        <source>Route style:</source>
        <translation>Стиль маршрута:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="140"/>
        <source>Routes</source>
        <translation>Маршруты</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="144"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="226"/>
        <source>Use anti-aliasing</source>
        <translation>Применять сглаживание</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="223"/>
        <source>Line width:</source>
        <translation>Толщина линии:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="108"/>
        <source>Colors</source>
        <translation>Цвета</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="39"/>
        <source>Always show the map</source>
        <translation>Всегда показывать карту</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="42"/>
        <source>Show the map even when no files are loaded.</source>
        <translation>Показывать карту, даже когда нет загруженных файлов.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="246"/>
        <source>Background color:</source>
        <translation>Цвет фона:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="257"/>
        <source>Paths</source>
        <translation>Треки</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="247"/>
        <source>Map opacity:</source>
        <translation>Прозрачность карты:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="260"/>
        <source>Map</source>
        <translation>Карты</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="259"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="468"/>
        <source>Graphs</source>
        <translation>Графики</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="45"/>
        <source>High-resolution</source>
        <translation>Высокое разрешение</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="46"/>
        <source>Standard</source>
        <translation>Стандартный</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="51"/>
        <source>Non-HiDPI maps are loaded as HiDPI maps. The map is sharp but map objects are small/hard to read.</source>
        <translation>Не-HiDPI карты загружаются как HiDPI карты. Карта резкая, но объекты карты маленькие/трудночитаемые.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="53"/>
        <source>Non-HiDPI maps are loaded such as they are. Map objects have the expected size but the map is blurry.</source>
        <translation>Не-HiDPI карты загружаются так, как есть. Объекты карты имеют ожидаемый размер, но карта размытая.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="86"/>
        <source>General</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="88"/>
        <source>HiDPI display mode</source>
        <translation>HiDPI режим отображения</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="122"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="138"/>
        <source>Width:</source>
        <translation>Толщина:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="123"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="139"/>
        <source>Style:</source>
        <translation>Стиль:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="176"/>
        <source>Waypoint color:</source>
        <translation>Цвет точки:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="177"/>
        <source>Waypoint size:</source>
        <translation>Размер точки:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="179"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="195"/>
        <source>Color:</source>
        <translation>Цвет:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="180"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="196"/>
        <source>Size:</source>
        <translation>Размер:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="181"/>
        <source>Waypoints</source>
        <translation>Точки</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="192"/>
        <source>POI color:</source>
        <translation>Цвет точки POI:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="193"/>
        <source>POI size:</source>
        <translation>Размер точки POI:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="197"/>
        <source>POIs</source>
        <translation>Точки POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="224"/>
        <source>Slider color:</source>
        <translation>Цвет ползунка:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="258"/>
        <source>Points</source>
        <translation>Точки</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="267"/>
        <source>Moving average window size</source>
        <translation>Ширины окна усреднения</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="286"/>
        <source>Elevation:</source>
        <translation>Высота:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="287"/>
        <source>Speed:</source>
        <translation>Скорость:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="288"/>
        <source>Heart rate:</source>
        <translation>Пульс:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="289"/>
        <source>Cadence:</source>
        <translation>Каденс:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="290"/>
        <source>Power:</source>
        <translation>Мощность:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="292"/>
        <source>Smoothing</source>
        <translation>Сглаживание</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="296"/>
        <source>Eliminate GPS outliers</source>
        <translation>Устранять GPS выбросы</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="302"/>
        <source>Outlier elimination</source>
        <translation>Устранение выбросов</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="309"/>
        <source>Smoothing:</source>
        <translation>Сглаживание:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="327"/>
        <source>mi/h</source>
        <translation>мл/ч</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="330"/>
        <source>kn</source>
        <translation>уз</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="333"/>
        <source>km/h</source>
        <translation>км/ч</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="337"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="491"/>
        <source>s</source>
        <translation>с</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="341"/>
        <source>Minimal speed:</source>
        <translation>Минимальная скорость:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="342"/>
        <source>Minimal duration:</source>
        <translation>Минимальная продолжительность:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="348"/>
        <source>Computed from distance/time</source>
        <translation>Вычисленная из расстояния/времени</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="349"/>
        <source>Recorded by device</source>
        <translation>Записанная устройством</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="364"/>
        <source>Filtering</source>
        <translation>Фильтрация</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="365"/>
        <source>Pause detection</source>
        <translation>Обнаружение остановок</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="366"/>
        <source>Speed</source>
        <translation>Скорость</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="378"/>
        <source>mi</source>
        <translation>мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="381"/>
        <source>nmi</source>
        <translation>мор. мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="384"/>
        <source>km</source>
        <translation>км</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="388"/>
        <source>POI radius:</source>
        <translation>Радиус точки POI:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="394"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="534"/>
        <source>POI</source>
        <translation>Точки POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="401"/>
        <source>WYSIWYG</source>
        <translation>WYSIWYG</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="402"/>
        <source>High-Resolution</source>
        <translation>Высокое разрешение</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="407"/>
        <source>The printed area is approximately the display area. The map zoom level does not change.</source>
        <translation>Печатная область примерно совпадает с областью отображения. Уровень приближения карты не изменяется.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="409"/>
        <source>The zoom level will be changed so that the whole content (tracks/waypoints) fits to the printed area and the map resolution is as close as possible to the print resolution.</source>
        <translation>Уровень приближения будет изменен так, чтобы всё содержимое (треки/точки) уместились в печатную область и разрешение карты было бы как можно ближе к разрешению печати.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="431"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="433"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="435"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="437"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="439"/>
        <source>Moving time</source>
        <translation>Время движения</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="441"/>
        <source>Item count (&gt;1)</source>
        <translation>Количество объектов (&gt;1)</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="456"/>
        <source>Separate graph page</source>
        <translation>Отдельная страница с графиком</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="466"/>
        <source>Print mode</source>
        <translation>Режим печати</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="467"/>
        <source>Header</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="475"/>
        <source>Use OpenGL</source>
        <translation>Использовать OpenGL</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="478"/>
        <source>Enable HTTP/2</source>
        <translation>Включить HTTP/2</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="485"/>
        <source>MB</source>
        <translation>МБ</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="495"/>
        <source>Image cache size:</source>
        <translation>Размер кэша изображений:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="496"/>
        <source>Connection timeout:</source>
        <translation>Таймаут соединения:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="512"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="537"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="530"/>
        <source>Appearance</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="532"/>
        <source>Maps</source>
        <translation>Карты</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="533"/>
        <source>Data</source>
        <translation>Данные</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="535"/>
        <source>Print &amp; Export</source>
        <translation>Печать и экспорт</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="564"/>
        <source>Options</source>
        <translation>Параметры</translation>
    </message>
</context>
<context>
    <name>PowerGraph</name>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="11"/>
        <source>W</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="12"/>
        <location filename="../src/GUI/powergraph.h" line="13"/>
        <source>Power</source>
        <translation>Мощность</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
</context>
<context>
    <name>PowerGraphItem</name>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="29"/>
        <location filename="../src/GUI/powergraphitem.cpp" line="31"/>
        <source>W</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
</context>
<context>
    <name>RouteItem</name>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="15"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="17"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="18"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
</context>
<context>
    <name>ScaleItem</name>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="108"/>
        <source>mi</source>
        <translation>мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="109"/>
        <location filename="../src/GUI/scaleitem.cpp" line="112"/>
        <source>ft</source>
        <translation>фт</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="111"/>
        <source>nmi</source>
        <translation>мор. мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="114"/>
        <source>km</source>
        <translation>км</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="115"/>
        <source>m</source>
        <translation>м</translation>
    </message>
</context>
<context>
    <name>SpeedGraph</name>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="16"/>
        <location filename="../src/GUI/speedgraph.h" line="14"/>
        <source>Speed</source>
        <translation>Скорость</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="102"/>
        <source>km/h</source>
        <translation>км/ч</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="29"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="26"/>
        <source>min/km</source>
        <translation>мин/км</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/mi</source>
        <translation>мин/мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/nmi</source>
        <translation>мин/мор.мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="31"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="33"/>
        <source>Pace</source>
        <translation>Темп</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="96"/>
        <source>kn</source>
        <translation>уз</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="99"/>
        <source>mi/h</source>
        <translation>мл/ч</translation>
    </message>
</context>
<context>
    <name>SpeedGraphItem</name>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>km/h</source>
        <translation>км/ч</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="31"/>
        <source>mi/h</source>
        <translation>мл/ч</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>kn</source>
        <translation>уз</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="35"/>
        <source>min/km</source>
        <translation>мин/км</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/mi</source>
        <translation>мин/мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/nmi</source>
        <translation>мин/мор.мл</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="39"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="41"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="43"/>
        <source>Pace</source>
        <translation>Темп</translation>
    </message>
</context>
<context>
    <name>TemperatureGraph</name>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="12"/>
        <location filename="../src/GUI/temperaturegraph.h" line="13"/>
        <source>Temperature</source>
        <translation>Температура</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="24"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="26"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="85"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="89"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>TemperatureGraphItem</name>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="36"/>
        <source>Average</source>
        <translation>Cреднее</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="38"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="40"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
</context>
<context>
    <name>TrackItem</name>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="13"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="15"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="16"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="18"/>
        <source>Total time</source>
        <translation>Общее время</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="20"/>
        <source>Moving time</source>
        <translation>Время движения</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="22"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
</context>
<context>
    <name>WaypointItem</name>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="18"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="19"/>
        <source>Coordinates</source>
        <translation>Координаты</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="22"/>
        <source>Elevation</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="25"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="28"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
</context>
</TS>
