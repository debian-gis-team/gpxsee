<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>CadenceGraph</name>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="11"/>
        <source>rpm</source>
        <translation>o/min</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="12"/>
        <location filename="../src/GUI/cadencegraph.h" line="13"/>
        <source>Cadence</source>
        <translation type="unfinished">Stegfrekvens</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
</context>
<context>
    <name>CadenceGraphItem</name>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="29"/>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="31"/>
        <source>rpm</source>
        <translation>o/min</translation>
    </message>
    <message>
        <location filename="../src/GUI/cadencegraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
</context>
<context>
    <name>Data</name>
    <message>
        <location filename="../src/data/data.cpp" line="116"/>
        <source>Supported files</source>
        <translation>Støttede filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>CSV files</source>
        <translation>CSV-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="118"/>
        <source>FIT files</source>
        <translation>FIT-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>GPX files</source>
        <translation>GPX-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="119"/>
        <source>IGC files</source>
        <translation>IGC-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>KML files</source>
        <translation>KML-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="120"/>
        <source>LOC files</source>
        <translation>LOC-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="121"/>
        <source>NMEA files</source>
        <translation>NMEA-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="122"/>
        <source>OziExplorer files</source>
        <translation>OziExplorer-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>SLF files</source>
        <translation>SLF-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="123"/>
        <source>TCX files</source>
        <translation>TCX-filer</translation>
    </message>
    <message>
        <location filename="../src/data/data.cpp" line="124"/>
        <source>All files</source>
        <translation>Alle filer</translation>
    </message>
</context>
<context>
    <name>ElevationGraph</name>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="48"/>
        <location filename="../src/GUI/elevationgraph.h" line="13"/>
        <source>Elevation</source>
        <translation>Høyde</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="59"/>
        <source>Ascent</source>
        <translation>Stigning</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="61"/>
        <source>Descent</source>
        <translation>Nedstigning</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="63"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="65"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="127"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraph.cpp" line="130"/>
        <source>ft</source>
        <translation>fot</translation>
    </message>
</context>
<context>
    <name>ElevationGraphItem</name>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="34"/>
        <source>ft</source>
        <translation>fot</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="37"/>
        <source>Ascent</source>
        <translation>Stigning</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="39"/>
        <source>Descent</source>
        <translation>Nedstigning</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="41"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/elevationgraphitem.cpp" line="43"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>PDF files</source>
        <translation>PDF-filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="25"/>
        <source>All files</source>
        <translation type="unfinished">Alle filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="52"/>
        <source>Portrait</source>
        <translation>Stående</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="53"/>
        <source>Landscape</source>
        <translation>Liggende</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="66"/>
        <source>in</source>
        <translation>tommer</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="94"/>
        <source>Page Setup</source>
        <translation>Sideinnstilling</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="97"/>
        <source>Page size:</source>
        <translation>Sidestørrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="98"/>
        <source>Resolution:</source>
        <translation>Oppløsning:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="99"/>
        <source>Orientation:</source>
        <translation>Sideretning:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="100"/>
        <source>Margins:</source>
        <translation>Marger:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="106"/>
        <source>File:</source>
        <translation>Fil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="113"/>
        <source>Output file</source>
        <translation>Utdatafil</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="120"/>
        <source>Export</source>
        <translation>Eksporter</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="135"/>
        <source>Export to PDF</source>
        <translation>Eksporter til PDF</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="142"/>
        <source>No output file selected.</source>
        <translation>Ingen utdatafil valgt.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="152"/>
        <source>%1 is a directory.</source>
        <translation>%1 er en mappe.</translation>
    </message>
    <message>
        <location filename="../src/GUI/exportdialog.cpp" line="157"/>
        <source>%1 is not writable.</source>
        <translation>%1 er skrivebeskyttet.</translation>
    </message>
</context>
<context>
    <name>FileSelectWidget</name>
    <message>
        <location filename="../src/GUI/fileselectwidget.cpp" line="38"/>
        <source>Select file</source>
        <translation>Velg fil</translation>
    </message>
</context>
<context>
    <name>Format</name>
    <message>
        <location filename="../src/GUI/format.cpp" line="54"/>
        <location filename="../src/GUI/format.cpp" line="61"/>
        <location filename="../src/GUI/format.cpp" line="84"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="57"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="64"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="68"/>
        <location filename="../src/GUI/format.cpp" line="81"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/format.cpp" line="71"/>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>GUI</name>
    <message>
        <location filename="../src/GUI/gui.cpp" line="199"/>
        <source>Quit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="206"/>
        <location filename="../src/GUI/gui.cpp" line="693"/>
        <location filename="../src/GUI/gui.cpp" line="694"/>
        <source>Paths</source>
        <translation>Stier</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="209"/>
        <location filename="../src/GUI/gui.cpp" line="659"/>
        <location filename="../src/GUI/gui.cpp" line="660"/>
        <source>Keyboard controls</source>
        <translation>Tastatursnarveier</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="212"/>
        <location filename="../src/GUI/gui.cpp" line="637"/>
        <source>About GPXSee</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="217"/>
        <source>Open...</source>
        <translation type="unfinished">Åpne…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="222"/>
        <source>Print...</source>
        <translation type="unfinished">Skriv ut…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="229"/>
        <source>Export to PDF...</source>
        <translation type="unfinished">Eksporter til PDF…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="235"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="241"/>
        <source>Reload</source>
        <translation>Last inn igjen</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="248"/>
        <source>Statistics...</source>
        <translation type="unfinished">Statistikk…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="256"/>
        <source>Load POI file...</source>
        <translation type="unfinished">Last inn POI-fil…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="260"/>
        <source>Close POI files</source>
        <translation>Lukk POI-filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="264"/>
        <source>Overlap POIs</source>
        <translation>Overlapp POI-er</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="269"/>
        <source>Show POI labels</source>
        <translation>Vis POI-etiketter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="274"/>
        <source>Show POIs</source>
        <translation>Vis POI-er</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="284"/>
        <source>Show map</source>
        <translation>Vis kart</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="292"/>
        <source>Load map...</source>
        <translation type="unfinished">Last inn kart…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="296"/>
        <source>Clear tile cache</source>
        <translation>Tøm flishurtiglager</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="301"/>
        <location filename="../src/GUI/gui.cpp" line="306"/>
        <location filename="../src/GUI/gui.cpp" line="676"/>
        <source>Next map</source>
        <translation>Neste kart</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="317"/>
        <source>Show tracks</source>
        <translation>Vis spor</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="322"/>
        <source>Show routes</source>
        <translation>Vis ruter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="327"/>
        <source>Show waypoints</source>
        <translation>Vis veipunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="332"/>
        <source>Waypoint labels</source>
        <translation>Veipunktetiketter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="337"/>
        <source>Route waypoints</source>
        <translation>Ruteveipunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="344"/>
        <source>Show graphs</source>
        <translation>Vis diagrammer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="354"/>
        <location filename="../src/GUI/gui.cpp" line="1004"/>
        <location filename="../src/GUI/gui.cpp" line="1069"/>
        <source>Distance</source>
        <translation>Avstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="361"/>
        <location filename="../src/GUI/gui.cpp" line="527"/>
        <location filename="../src/GUI/gui.cpp" line="1007"/>
        <location filename="../src/GUI/gui.cpp" line="1071"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="368"/>
        <source>Show grid</source>
        <translation>Vis rutenett</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="373"/>
        <source>Show slider info</source>
        <translation>Vis linjalinfo</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="380"/>
        <source>Show toolbars</source>
        <translation>Vis verktøylinjer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="387"/>
        <source>Total time</source>
        <translation>Total tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="393"/>
        <location filename="../src/GUI/gui.cpp" line="1009"/>
        <location filename="../src/GUI/gui.cpp" line="1073"/>
        <source>Moving time</source>
        <translation>Tid i bevegelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="401"/>
        <source>Metric</source>
        <translation>Metrisk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="407"/>
        <source>Imperial</source>
        <translation>Imperial</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="413"/>
        <source>Nautical</source>
        <translation>Nautisk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="421"/>
        <source>Decimal degrees (DD)</source>
        <translation type="unfinished">Desimalgrader (DD)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="427"/>
        <source>Degrees and decimal minutes (DMM)</source>
        <translation type="unfinished">Desimalgrader og desimalminutter (DMM)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="434"/>
        <source>Degrees, minutes, seconds (DMS)</source>
        <translation type="unfinished">Grader, minutter, sekunder (DMS)</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="440"/>
        <source>Fullscreen mode</source>
        <translation>Fullskjermsmodus</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="447"/>
        <source>Options...</source>
        <translation type="unfinished">Valg…</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="453"/>
        <source>Next</source>
        <translation>Neste</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="457"/>
        <source>Previous</source>
        <translation>Forrige</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="461"/>
        <source>Last</source>
        <translation>Siste</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="465"/>
        <source>First</source>
        <translation>Første</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="473"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="488"/>
        <source>&amp;Map</source>
        <translation>&amp;Kart</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="496"/>
        <source>&amp;Graph</source>
        <translation>&amp;Graf</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="505"/>
        <source>&amp;POI</source>
        <translation>&amp;POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="506"/>
        <source>POI files</source>
        <translation>POI-filer</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="517"/>
        <source>&amp;Data</source>
        <translation>&amp;Data</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="518"/>
        <source>Display</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="526"/>
        <source>&amp;Settings</source>
        <translation>&amp;Innstillinger</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="530"/>
        <source>Units</source>
        <translation>Enheter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="534"/>
        <source>Coordinates format</source>
        <translation type="unfinished">Koordinatformat</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="544"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="560"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="567"/>
        <source>Show</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="573"/>
        <source>Navigation</source>
        <translation>Navigasjon</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="638"/>
        <source>Version %1</source>
        <translation>Versjon %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="642"/>
        <source>GPXSee is distributed under the terms of the GNU General Public License version 3. For more info about GPXSee visit the project homepage at %1.</source>
        <translation>GPXSee er distribuert i henhold til vilkårene i GNU general public lisens, versjon 3. For mer info om GPXSee, besøk prosjekthjemmesiden på %1.</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="663"/>
        <source>Next file</source>
        <translation>Neste fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="664"/>
        <source>Previous file</source>
        <translation>Forrige fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="666"/>
        <source>First file</source>
        <translation>Første fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="668"/>
        <source>Last file</source>
        <translation>Siste fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="669"/>
        <source>Append file</source>
        <translation>Legg til fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="670"/>
        <source>Next/Previous</source>
        <translation>Neste/forrige</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="672"/>
        <source>Toggle graph type</source>
        <translation>Veksle diagramtype</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="674"/>
        <source>Toggle time type</source>
        <translation>Veksle tidstype</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="678"/>
        <source>Previous map</source>
        <translation>Forrige kart</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="679"/>
        <source>Zoom in</source>
        <translation>Forstørr</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="681"/>
        <source>Zoom out</source>
        <translation>Forminsk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="683"/>
        <source>Digital zoom</source>
        <translation>Digital forstørrelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="684"/>
        <source>Zoom</source>
        <translation>Forstørr</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="703"/>
        <source>Tile cache directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="697"/>
        <source>Map directory:</source>
        <translation>Kartmappe:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="699"/>
        <source>POI directory:</source>
        <translation>POI-mappe:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="701"/>
        <source>GCS/PCS directory:</source>
        <translation>GCS-/PCS-mappe:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="712"/>
        <source>Open file</source>
        <translation>Åpne fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="806"/>
        <source>Error loading data file:</source>
        <translation>Feil ved innlasting av datafil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="809"/>
        <location filename="../src/GUI/gui.cpp" line="844"/>
        <source>Line: %1</source>
        <translation>Linje: %1</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="817"/>
        <source>Open POI file</source>
        <translation>Åpne POI-fil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="841"/>
        <source>Error loading POI file:</source>
        <translation>Kunne ikke laste inn POI-fil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="981"/>
        <location filename="../src/GUI/gui.cpp" line="1049"/>
        <source>Tracks</source>
        <translation>Spor</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="984"/>
        <location filename="../src/GUI/gui.cpp" line="1051"/>
        <source>Routes</source>
        <translation>Ruter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="987"/>
        <location filename="../src/GUI/gui.cpp" line="1053"/>
        <source>Waypoints</source>
        <translation>Veipunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="993"/>
        <location filename="../src/GUI/gui.cpp" line="997"/>
        <location filename="../src/GUI/gui.cpp" line="1059"/>
        <location filename="../src/GUI/gui.cpp" line="1062"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1030"/>
        <location filename="../src/GUI/gui.cpp" line="1031"/>
        <source>Statistics</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1045"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1272"/>
        <source>Open map file</source>
        <translation>Åpne kartfil</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1295"/>
        <source>Error loading map:</source>
        <translation>Feil ved innlasting av kart:</translation>
    </message>
    <message>
        <location filename="../src/GUI/gui.cpp" line="1306"/>
        <source>No files loaded</source>
        <translation>Ingen filer lastet opp</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/GUI/gui.cpp" line="1310"/>
        <source>%n files</source>
        <translation type="unfinished">
            <numerusform>%n fil</numerusform>
            <numerusform>%n filer</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>GearRatioGraph</name>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="12"/>
        <location filename="../src/GUI/gearratiograph.h" line="14"/>
        <source>Gear ratio</source>
        <translation>Utveksling</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="22"/>
        <source>Most used</source>
        <translation>Mest brukt</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="24"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiograph.cpp" line="26"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
</context>
<context>
    <name>GearRatioGraphItem</name>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="44"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="45"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/gearratiographitem.cpp" line="46"/>
        <source>Most used</source>
        <translation>Mest brukt</translation>
    </message>
</context>
<context>
    <name>GraphView</name>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="46"/>
        <source>Data not available</source>
        <translation>Data ikke tilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="66"/>
        <location filename="../src/GUI/graphview.cpp" line="181"/>
        <source>Distance</source>
        <translation>Avsntand</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="111"/>
        <location filename="../src/GUI/graphview.cpp" line="119"/>
        <source>ft</source>
        <translation>fot</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="114"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="122"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="127"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="130"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="136"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="139"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="142"/>
        <source>h</source>
        <translation>t</translation>
    </message>
    <message>
        <location filename="../src/GUI/graphview.cpp" line="183"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
</context>
<context>
    <name>HeartRateGraph</name>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="11"/>
        <source>bpm</source>
        <translation type="unfinished">s/m</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="12"/>
        <location filename="../src/GUI/heartrategraph.h" line="13"/>
        <source>Heart rate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
</context>
<context>
    <name>HeartRateGraphItem</name>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="29"/>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="31"/>
        <source>bpm</source>
        <translation>s/m</translation>
    </message>
    <message>
        <location filename="../src/GUI/heartrategraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
</context>
<context>
    <name>MapList</name>
    <message>
        <location filename="../src/map/maplist.cpp" line="117"/>
        <source>Supported files</source>
        <translation>Støttede filer</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="119"/>
        <source>MBTiles maps</source>
        <translation>MBTiles-kart</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="120"/>
        <source>Garmin JNX maps</source>
        <translation>Garmin JNX-kart</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="121"/>
        <source>OziExplorer maps</source>
        <translation>OziExplorer-kart</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="122"/>
        <source>TrekBuddy maps/atlases</source>
        <translation>TrekBuddy kart/atlas</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="123"/>
        <source>GeoTIFF images</source>
        <translation>GeoTIFF-bilder</translation>
    </message>
    <message>
        <location filename="../src/map/maplist.cpp" line="124"/>
        <source>Online map sources</source>
        <translation>Nettbaserte kartkilder</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="39"/>
        <source>Always show the map</source>
        <translation>Alltid vis kartet</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="42"/>
        <source>Show the map even when no files are loaded.</source>
        <translation>Vis kartet selv når ingen filer er innlastet.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="45"/>
        <source>High-resolution</source>
        <translation>Høyoppløsning</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="46"/>
        <source>Standard</source>
        <translation>Forvalg</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="51"/>
        <source>Non-HiDPI maps are loaded as HiDPI maps. The map is sharp but map objects are small/hard to read.</source>
        <translation>Ikke-HiDPI -kart lastes som HiDPI-kart. Kartet er skarpt, men kartobjekter kan være små/vanskelige å lese.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="53"/>
        <source>Non-HiDPI maps are loaded such as they are. Map objects have the expected size but the map is blurry.</source>
        <translation>Ikke-HiDPI -kart lastes som de er. Kartobjekter har forventet størrelse, men kartet er uskarpt.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="86"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="88"/>
        <source>HiDPI display mode</source>
        <translation>HiDPI-visningsmodus</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="105"/>
        <source>Base color:</source>
        <translation>Grunnfarge:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="106"/>
        <source>Palette shift:</source>
        <translation type="unfinished">Palettforskyvning:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="108"/>
        <source>Colors</source>
        <translation type="unfinished">Farger</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="119"/>
        <source>Track width:</source>
        <translation type="unfinished">Sporbredde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="120"/>
        <source>Track style:</source>
        <translation type="unfinished">Sporstil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="122"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="138"/>
        <source>Width:</source>
        <translation>Bredde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="123"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="139"/>
        <source>Style:</source>
        <translation>Stil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="124"/>
        <source>Tracks</source>
        <translation>Spor</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="135"/>
        <source>Route width:</source>
        <translation>Rutebredde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="136"/>
        <source>Route style:</source>
        <translation>Rutestil:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="140"/>
        <source>Routes</source>
        <translation>Ruter</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="144"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="226"/>
        <source>Use anti-aliasing</source>
        <translation>Bruk kanutjevning</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="176"/>
        <source>Waypoint color:</source>
        <translation>Veipunktsfarge:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="177"/>
        <source>Waypoint size:</source>
        <translation>Veipunktsstørrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="179"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="195"/>
        <source>Color:</source>
        <translation>Farge:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="180"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="196"/>
        <source>Size:</source>
        <translation>Størrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="181"/>
        <source>Waypoints</source>
        <translation>Veipunkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="192"/>
        <source>POI color:</source>
        <translation>POI-farge:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="193"/>
        <source>POI size:</source>
        <translation>POI-størrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="197"/>
        <source>POIs</source>
        <translation>POI-er</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="223"/>
        <source>Line width:</source>
        <translation>Linjebredde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="224"/>
        <source>Slider color:</source>
        <translation type="unfinished">Linjalfarge:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="246"/>
        <source>Background color:</source>
        <translation>Bakgrunnsfarge:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="247"/>
        <source>Map opacity:</source>
        <translation>Kartdekkevne:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="257"/>
        <source>Paths</source>
        <translation>Stier</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="258"/>
        <source>Points</source>
        <translation>Punkter</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="259"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="468"/>
        <source>Graphs</source>
        <translation>Diagrammer</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="260"/>
        <source>Map</source>
        <translation>Kart</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="267"/>
        <source>Moving average window size</source>
        <translation>Flytter gjennomsnittlig kartstørrelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="286"/>
        <source>Elevation:</source>
        <translation>Høyde:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="287"/>
        <source>Speed:</source>
        <translation>Hastighet:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="288"/>
        <source>Heart rate:</source>
        <translation>Puls:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="289"/>
        <source>Cadence:</source>
        <translation>Stegfrekvens:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="290"/>
        <source>Power:</source>
        <translation>Kraft:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="292"/>
        <source>Smoothing</source>
        <translation>Utjevning</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="296"/>
        <source>Eliminate GPS outliers</source>
        <translation>Eliminer GPS-ekstremverdier</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="302"/>
        <source>Outlier elimination</source>
        <translation>Ekstermverdieliminering</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="309"/>
        <source>Smoothing:</source>
        <translation>Utjevning:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="327"/>
        <source>mi/h</source>
        <translation>mi/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="330"/>
        <source>kn</source>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="333"/>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="337"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="491"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="341"/>
        <source>Minimal speed:</source>
        <translation>Minimumshastighet:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="342"/>
        <source>Minimal duration:</source>
        <translation>Minimumsvarighet:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="348"/>
        <source>Computed from distance/time</source>
        <translation>Utregnet fra avstand/tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="349"/>
        <source>Recorded by device</source>
        <translation>Tatt opp av enhet</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="364"/>
        <source>Filtering</source>
        <translation>Filtrering</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="365"/>
        <source>Pause detection</source>
        <translation>Sett oppdagelse på pause</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="366"/>
        <source>Speed</source>
        <translation>Hastighet</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="378"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="381"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="384"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="388"/>
        <source>POI radius:</source>
        <translation>POI-radius:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="394"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="534"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="401"/>
        <source>WYSIWYG</source>
        <translation>WYSIWYG</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="402"/>
        <source>High-Resolution</source>
        <translation>Høyoppløsning</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="407"/>
        <source>The printed area is approximately the display area. The map zoom level does not change.</source>
        <translation>Det utskrevne området er omentrent det samme som visningsområdet. Kartforstørrelsesnivået endrer seg ikke.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="409"/>
        <source>The zoom level will be changed so that the whole content (tracks/waypoints) fits to the printed area and the map resolution is as close as possible to the print resolution.</source>
        <translation>Forstørrelsesnivået vil endres slik at hele innholdet (spor/veipunkter) passer med utskrevet område, og kartoppløsningen er så lik utskriftsoppløsningen som mulig.</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="431"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="433"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="435"/>
        <source>Distance</source>
        <translation>Avstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="437"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="439"/>
        <source>Moving time</source>
        <translation>Tid i bevegelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="441"/>
        <source>Item count (&gt;1)</source>
        <translation>Elementantall (&gt;1)</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="456"/>
        <source>Separate graph page</source>
        <translation>Separat diagramside</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="466"/>
        <source>Print mode</source>
        <translation>Utskriftsmodus</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="467"/>
        <source>Header</source>
        <translation>Topptekst</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="475"/>
        <source>Use OpenGL</source>
        <translation>Bruk OpenGL</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="478"/>
        <source>Enable HTTP/2</source>
        <translation>Skru på HTTP/2</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="485"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="495"/>
        <source>Image cache size:</source>
        <translation>Bildehurtiglagringstørrelse:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="496"/>
        <source>Connection timeout:</source>
        <translation>Tilkoblingstidsavbrudd:</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="512"/>
        <location filename="../src/GUI/optionsdialog.cpp" line="537"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="530"/>
        <source>Appearance</source>
        <translation>Utseende</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="532"/>
        <source>Maps</source>
        <translation>Kart</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="533"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="535"/>
        <source>Print &amp; Export</source>
        <translation>Utskrift og ekport</translation>
    </message>
    <message>
        <location filename="../src/GUI/optionsdialog.cpp" line="564"/>
        <source>Options</source>
        <translation>Valg</translation>
    </message>
</context>
<context>
    <name>PowerGraph</name>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="11"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="12"/>
        <location filename="../src/GUI/powergraph.h" line="13"/>
        <source>Power</source>
        <translation>Kraft</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraph.cpp" line="24"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
</context>
<context>
    <name>PowerGraphItem</name>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="28"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="29"/>
        <location filename="../src/GUI/powergraphitem.cpp" line="31"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../src/GUI/powergraphitem.cpp" line="30"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
</context>
<context>
    <name>RouteItem</name>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="15"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="17"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/routeitem.cpp" line="18"/>
        <source>Distance</source>
        <translation>Avstand</translation>
    </message>
</context>
<context>
    <name>ScaleItem</name>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="108"/>
        <source>mi</source>
        <translation>mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="109"/>
        <location filename="../src/GUI/scaleitem.cpp" line="112"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="111"/>
        <source>nmi</source>
        <translation>nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="114"/>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../src/GUI/scaleitem.cpp" line="115"/>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>SpeedGraph</name>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="16"/>
        <location filename="../src/GUI/speedgraph.h" line="14"/>
        <source>Speed</source>
        <translation>Hastighet</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="26"/>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/mi</source>
        <translation type="unfinished">min/mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="27"/>
        <source>min/nmi</source>
        <translation type="unfinished">min/nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="29"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="31"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="33"/>
        <source>Pace</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="96"/>
        <source>kn</source>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="99"/>
        <source>mi/h</source>
        <translation>mi/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraph.cpp" line="102"/>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
</context>
<context>
    <name>SpeedGraphItem</name>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="31"/>
        <source>mi/h</source>
        <translation>mi/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>kn</source>
        <translation>kn</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="32"/>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="35"/>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/mi</source>
        <translation>min/mi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="36"/>
        <source>min/nmi</source>
        <translation type="unfinished">min/nmi</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="39"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="41"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../src/GUI/speedgraphitem.cpp" line="43"/>
        <source>Pace</source>
        <translation>Tempo</translation>
    </message>
</context>
<context>
    <name>TemperatureGraph</name>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="12"/>
        <location filename="../src/GUI/temperaturegraph.h" line="13"/>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="22"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="24"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="26"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="85"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraph.cpp" line="89"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>TemperatureGraphItem</name>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="33"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="36"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="38"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../src/GUI/temperaturegraphitem.cpp" line="40"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
</context>
<context>
    <name>TrackItem</name>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="13"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="15"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="16"/>
        <source>Distance</source>
        <translation>Avstand</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="18"/>
        <source>Total time</source>
        <translation>Total tid</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="20"/>
        <source>Moving time</source>
        <translation>Tid i bevegelse</translation>
    </message>
    <message>
        <location filename="../src/GUI/trackitem.cpp" line="22"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
</context>
<context>
    <name>WaypointItem</name>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="18"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="19"/>
        <source>Coordinates</source>
        <translation>Koordinater</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="22"/>
        <source>Elevation</source>
        <translation>Høyde</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="25"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../src/GUI/waypointitem.cpp" line="28"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
</context>
</TS>
