Source: gpxsee
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Luboš Novák <alvinx12@centrum.cz>
Section: science
Priority: optional
Build-Depends: debhelper (>= 11),
               libqt5opengl5-dev,
               qt5-qmake,
               qtbase5-dev,
               qtbase5-dev-tools,
               qttools5-dev-tools
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/debian-gis-team/gpxsee
Vcs-Git: https://salsa.debian.org/debian-gis-team/gpxsee.git
Homepage: https://www.gpxsee.org

Package: gpxsee
Architecture: any
Depends: gpxsee-data (= ${source:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: GPS log files visualizing and analyzing tool
 GPXSee is a Qt based tool for visualizing and analyzing GPX, TCX, FIT, KML,
 IGC, NMEA and Garmin CSV files.
 .
 Its main features are:
  * User-definable online maps (OSM/Google tiles, WMTS, WMS)
  * Offline maps (OziExplorer maps, TrekBuddy maps/atlases, GeoTIFF images)
  * Elevation, speed, heart rate, cadence, power and temperature graphs
  * Support for multiple tracks in one view
  * Support for POI files
  * Print/export to PDF
  * Full-screen mode
  * Opens GPX, TCX, FIT, KML, IGC, NMEA and Garmin CSV files

Package: gpxsee-data
Architecture: all
Depends: ${misc:Depends}
Description: GPS log files visualizing and analyzing tool - data
 GPXSee is a Qt based tool for visualizing and analyzing GPX, TCX, FIT, KML,
 IGC, NMEA and Garmin CSV files.
 .
 This package includes the architecture-independent data for GPXSee.
